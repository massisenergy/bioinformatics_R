---
title: 'R Notebook: eBird.Rmd'
author: "SM"
date: "`r format(Sys.time(), '%d %B, %Y')`"
geometry: margin=.5in, bottom=.75in
urlcolor: blue
output:
    html_document:
    # df_print: paged
    toc: yes
toc_depth: 2
toc_float: yes
html_notebook:
    fig_caption: no
theme: flatly
toc: yes
toc_depth: 2
toc_float: yes
# df_print: kable
pdf_document:
    # df_print: kable
    highlight: tango
latex_engine: xelatex
toc: yes
---

```{r, setup, include=FALSE}
knitr::opts_chunk$set(
  comment = '', fig.width = 2.5, fig.height = 2.5
)
```

#### Multiple versions of BioConductor and R
 - use `rig` and `rswitch` [Managing multiple versions](https://cran.r-project.org/web/packages/BiocManager/vignettes/BiocManager.html#multiple-versions)
 - [Installing multiple parallel versions of R on Mac and getting back up and running quickly by simplifying package installation](https://jacobrprice.github.io/2019/09/19/Installing-multiple-parallel-R-versions.html)
 - [Uninstalling under macOS](https://cran.rstudio.org/doc/manuals/R-admin.html#Uninstalling-under-macOS-1)
  
Before installing another version of R, first do this (otherwise the current version will be overwritten)
```{zsh}
ls -l
# sudo pkgutil --forget org.R-project.arm64.R.fw.pkg 
```


#### Load required packages and data ---------------------------------------------

```{r loadPackagesDirectory, include=T, echo=F, message=F}
library(here); getwd(); 
library(tidyverse); library(ggplot2); library(kableExtra);

source(file = 
"~/Library/CloudStorage/OneDrive-MSFT/Coding_oNLYiNsHAREPOINT/R/Load_packages.R");

knitr::opts_knit$set(root.dir = "~/Library/CloudStorage/OneDrive-MSFT/Coding_oNLYiNsHAREPOINT/R/bioinformatics_R")
knitr::opts_chunk$set(echo = TRUE);
getwd();
```

#### install Bioconductor and check version ---------------------------------------

```{r }
if (!requireNamespace("BiocManager", quietly = TRUE))
    install.packages("BiocManager")
BiocManager::install(version = "3.19")
BiocManager::version()
```

#### install Bioconductor packages-------------------------------------------------

```{r }
BiocManager::install("BSgenome.Hsapiens.UCSC.hg19")
BiocManager::install(c("genefilter","geneplotter"))
```

#### Load installed packages ------------------------------------------------------

```{r }
library(BSgenome.Hsapiens.UCSC.hg19)
library(genefilter)
library(geneplotter)
```

#### get help through the documentation

```{r}
help.start()
?mean
help(mean)
help(package="genefilter")
```

#### inspect objects, classes and methods

```{r}
library(Biobase)    # load one of the core Bioconductor packages
?ExpressionSet
?"ExpressionSet-class"
methods(class = ExpressionSet)
```

#### inspect the source code for functions and methods

```{r}
read.csv
plotMA
showMethods("plotMA")
getMethod("plotMA","data.frame")
```

#### vignettes teach you how to use various functions in a package

```{r}
vignette(package="Biobase")
vignette("ExpressionSetIntroduction")
browseVignettes(package="Biobase")
```

#### report key details about your R session

```{r}
sessionInfo()
```

### Section 1: What we measure, why and how

#### What we measure and why

```{r}
BiocManager::install(c("genefu", "gwascat", "hgu133a.db"))
BiocManager::available("COPDSexualDimorphism"); 
BiocManager::install("COPDSexualDimorphism.data"); 
packageVersion("COPDSexualDimorphism.data")
# BiocManager::available( "genomicsclass/tissuesGeneExpression")
```
##### 

```{r}
BiocManager::install("iC10", force = T)
BiocManager::install("genefu")
library(genefu)
data(sig.gene70)
dim(sig.gene70)
head(sig.gene70)[,1:6]
```

Tried with -
 - R v_4.4.1, BiocManager v_3.19 : "Error: package ‘iC10’ required by ‘genefu’ could not be found"
 - R v_4.2-arm64 (4.2.0), BiocManager v_3.15 : suggested by the course
 - R v_4.2 (4.2.3), BiocManager v_13 : proabably x86_64
None of these are without errors. The course being over 6 years old (2019), it might not be worthwhile to pursue further!