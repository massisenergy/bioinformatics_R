---
title: "Week 1"
format: html
editor: visual
engine: knitr
date: "`r format(Sys.time(), '%d %B, %Y')`"
author: 
  - name: massisenergy
    id: jc
    orcid: 0000-0000-0000-0000
    email: 
    affiliation: 
      - name: University
        city: Providence
        state: RI
        url: www.ac.edu
abstract: > 
  Introduction to sequence, quality, control, 
  mapping, and variant calling.
keywords:
  - MacOS 15
  - zsh
  - arm64
  - python 3.9
license: "CC BY"
copyright: 
  holder: massisenergy
  year: 2024
citation: 
  container-title: none
  volume: 0
  issue: 0
  doi: 10.5555/12345678
funding: "The author received no specific funding for this work."
---

#### Quarto

Quarto enables you to weave together content and executable code into a finished document. To learn more about Quarto see <https://quarto.org>.

# Week 1:

## **1.8 Introduction to software management using Conda**

Homebrew installation of the required packages work but these are not sufficient to work with nextflow in the Week 2. Miniconda 3 doesn't work with the provided `MOOC.yml` file, which needs to be modified (thanks to suggestions by [***Ayman Khalifa***](https://www.futurelearn.com/profiles/21618318)) as attached in this directory.

```{zsh, engine.opts='-l'}
#|eval: false
#|include: false

pwd
mkdir -p Analysis Data Exploration Plots Results
ls -l
```

### Conda configuration and environment creation using the `MOOC.yml`

```{zsh, engine.opts='-l'}
#|eval: false
#|include: false

conda config --add channels conda-forge 
conda config --add channels bioconda 
conda config --add channels defaults

conda env create -n MOOC --file MOOC.yml
conda activate MOOC
```

## 1.14 Introduction to course dataset

This installation of miniconda in the current setup doesn't provide `sratools`. Thanks to [***Ara Abbott***](https://www.futurelearn.com/profiles/21610790) who commented on the manual installation and running of `fastq-dump` as following:

-   First, download sratoolkit from here: <https://github.com/ncbi/sra-tools/wiki/01.-Downloading-SRA-Toolkit>

```{zsh, include=false, eval=false,}

tar -vxzf sratoolkit.3.1.1-mac-arm64.tar.gz
export PATH=$PATH:$PWD/sratoolkit.3.1.1-mac64/bin #to let the shell know about the executables
cd sratoolkit.3.1.1-mac-arm64/bin
xattr -dr com.apple.quarantine "vdb-config" #this needs to be done for other executables if MacOS blocks them
./vdb-config -i #select both options, save and exit
./fastq-dump --stdout -X 2 SRR390728 #testing as suggested: https://github.com/ncbi/sra-tools/wiki/02.-Installing-SRA-Toolkit#5-proceed-to-the-quick-configuration-guide
./fastq-dump --split-files ERR5743893
```

## 1.17 Exercise and discussion

```{zsh, engine.opts='-l', include=false}

mkdir -p Results/QC_Reports
fastqc Data/RR5743893_1.fastq Data/ERR5743893_2.fastq --outdir Results/QC_Reports
multiqc Results/QC_Reports --outdir Exploration
```

## 1.19 Exercise and discussion

```{zsh, include=false}

mkdir -p Analysis/Mapping
mv ~/Downloads/MN908947.fasta Data/
samtools faidx Data/MN908947.fasta
bwa index Data/MN908947.fasta
bwa mem Data/MN908947.fasta Data/ERR5743893_1.fastq Data/ERR5743893_2.fastq > Analysis/Mapping/ERR5743893.sam
cd Analysis/Mapping
ls -lhrt
samtools view -@ 30 -S -b Analysis/Mapping/ERR5743893.sam > Analysis/Mapping/ERR5743893.bam
samtools sort -@ 32 -o Analysis/Mapping/ERR5743893_sorted.bam Analysis/Mapping/ERR5743893.bam
```

## 1.21 Exercise and discussion

❌ Couldn't install freebayes using *conda* with this setup. ✅ Installed using *homebrew*

```{zsh, include=false}
brew install freebayes
which freebayes; freebayes --version

freebayes -f Data/MN908947.fasta Analysis/Mapping/ERR5743893_sorted.bam > Analysis/VariantCalling/ERR5743893.vcf

/opt/homebrew/Cellar/bcftools/1.20_1/bin/bcftools query -f '%TYPE\n' Analysis/VariantCalling/ERR5743893.vcf.gz | sort | uniq -c
```
